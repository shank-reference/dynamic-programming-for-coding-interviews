def tower_of_hanoi(start: str, dest: str, extra: str, n: int):
    if n == 0:
        return
    tower_of_hanoi(start, extra, dest, n - 1)
    print("moving {} from {} to {}".format(n, start, dest))
    tower_of_hanoi(extra, dest, start, n - 1)


tower_of_hanoi("A", "B", "C", 5)
