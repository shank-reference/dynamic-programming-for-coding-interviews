from typing import List
from collections import defaultdict


def pairs_sum(arr: List[int], x: int) -> int:
    m = defaultdict(int)
    for i in arr:
        m[i] += 1

    count = 0

    for i in arr:
        if x - i in m:
            count += 1

    return count
