def combinations_count_recursive(n, m):
    if n is 0 or m is 0 or n == m:
        return 1
    return combinations_count_recursive(
        n - 1, m - 1) + combinations_count_recursive(n - 1, m)


print(combinations_count_recursive(5, 4))
print(combinations_count_recursive(5, 3))
print(combinations_count_recursive(5, 2))
