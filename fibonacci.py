def fib_recursive(n: int):
    if n == 0 or n == 1:
        return n
    return fib_recursive(n - 1) + fib_recursive(n - 2)


def fib_iterative(n: int):
    if n == 0 or n == 1:
        return n
    a, b = 0, 1
    for _ in range(2, n + 1):
        b, a = a + b, b
    return b


assert fib_recursive(2) == 1
assert fib_recursive(5) == 5
assert fib_recursive(10) == 55

assert fib_iterative(2) == 1
assert fib_iterative(5) == 5
assert fib_iterative(10) == 55


def I(n):
    result = []
    for i in range(n):
        row = [0 for _ in range(i)] + [1] + [0 for _ in range(n - i - 1)]
        result.append(row)
    return result


def matrix_mul(A, B):
    # A and B are matrices in the form of list of lists (2D array)
    if len(A) is 0 or len(B) is 0:
        raise ValueError("only non-empty matrices can be multiplied")

    if len(A[0]) != len(B):
        raise ValueError(
            "number of columns in A should be equal to number of rows in B")

    result = []
    a_rows = len(A)
    a_cols = len(A[0])
    b_cols = len(B[0])
    for r in range(a_rows):
        row = []
        for c in range(b_cols):
            val = sum(A[r][i] * B[i][c] for i in range(a_cols))
            row.append(val)
        result.append(row)
    return result


def matrix_power_final(A, x):
    result = I(len(A))
    cur_A = A
    while x > 0:
        if x % 2 == 1:
            result = matrix_mul(result, cur_A)
        cur_A = matrix_mul(cur_A, cur_A)
        x = x // 2
    return result


def fib_logn(n):
    # TODO
    pass
