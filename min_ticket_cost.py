# 4 stations -> [0, 1, 2, 3]

cost = [
    [0, 10, 75, 94],
    [-1, 0, 0, 80],
    [-1, -1, 0, 80],
    [-1, -1, -1, 0]
]

def min_cost(s, d):
    if s == d or s == d-1:
        return cost[s][d]

    # try every intermediate station to find min ocst
    mc = cost[s][d]
    for i in range(s+1, d):
        # min cost of going from s to i
        # and min cost of going from i to d
        mc = min(mc, min_cost(s, i) + min_cost(i, d))
    return mc

print(min_cost(0, 3))
print(min_cost(1, 3))

from math import inf

memo = [[inf for _ in range(len(cost[0]))] for _ in range(len(cost))]

def min_cost_memoized(s, d):
    if s == d or s == d-1:
        return cost[s][d]

    if memo[s][d] is inf:
        mc = cost[s][d]
        for i in range(s+1, d):
            mc = min(mc,
                     min_cost_memoized(s, i) + min_cost_memoized(i, d))
        memo[s][d] = mc

    return memo[s][d]

print(min_cost_memoized(0, 3))
print(min_cost_memoized(1, 3))
