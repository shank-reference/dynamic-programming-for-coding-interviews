from math import inf
from typing import List


def knapsack_recursive(c: int, w: List[int], v: List[int]) -> int:
    if not w or c <= 0:
        return 0

    if w[-1] > c:
        return knapsack_recursive(c, w[:-1], v[:-1])

    # last item is not included in the knapsack
    max_excluding = knapsack_recursive(c, w[:-1], v[:-1])

    # last item is included in the knapsack
    max_including = v[-1] + knapsack_recursive(c - w[-1], w[:-1], v[:-1])

    return max(max_including, max_excluding)


W = [1, 3, 4, 5]
V = [1, 3, 2, 5]
C = 8
print(knapsack_recursive(C, W, V))

cache = {}


def knapsack_memoized(c: int, w: List[int], v: List[int]) -> int:
    m = len(w)
    if (c, m) in cache:
        return cache[(c, m)]

    if not w or c <= 0:
        return 0

    if w[-1] > c:
        return knapsack_memoized(c, w[:-1], v[:-1])

    max_excluding = knapsack_memoized(c, w[:-1], v[:-1])
    max_including = v[-1] + knapsack_memoized(c - w[-1], w[:-1], v[:-1])
    cache[(c, m)] = max(max_including, max_excluding)
    return cache[(c, m)]


print(knapsack_memoized(C, W, V))


def knapsack_dp(c: int, w: List[int], v: List[int]) -> int:
    m = len(w)
    kp = [[-inf for _ in range(m + 1)] for _ in range(c + 1)]

    for col in range(m + 1):
        kp[0][col] = 0

    for row in range(c + 1):
        kp[row][0] = 0

    for cp in range(1, c + 1):
        for i in range(1, m + 1):
            if w[i - 1] > cp:
                kp[cp][i] = kp[c][i - 1]
            else:
                kp[cp][i] = max(v[i - 1] + kp[cp - w[i - 1]][i], kp[cp][i - 1])

    return kp[c][m]


print(knapsack_dp(C, W, V))