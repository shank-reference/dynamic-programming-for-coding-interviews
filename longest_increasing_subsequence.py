from typing import List


def lis(arr: List[int]) -> List[int]:
    m = len(arr)
    if m == 0:
        return []
    if m == 1:
        return arr

    list_at = []

    for i, v in enumerate(arr):
        if i == 0 or v < arr[i - 1]:
            list_at.append([v])
        else:
            lat = []
            for j in range(i):
                if arr[j] < v and len(lat) <= len(list_at[j]):
                    lat = list_at[j] + [v]
            list_at.append(lat)
    return max(list_at, key=len)


A = [1, 2, 3, 1, 5, 3]
print(lis(A))