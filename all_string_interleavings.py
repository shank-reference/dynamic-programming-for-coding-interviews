from typing import List

def interleavings_recursive(a: str, b: str) -> List[str]:
    if not a and not b:
        return []

    if not a:
        return [b]

    if not b:
        return [a]

    result = [a[0]+s for s in interleavings_recursive(a[1:], b)]
    result += [b[0]+s for s in interleavings_recursive(a, b[1:])]
    return  result


A = "AB"
B = "XY"
print(interleavings_recursive(A, B))


cache = {}
def interleavings_memoized(a: str, b: str) -> List[str]:
    if (a, b) in cache:
        return cache[(a, b)]

    if not a and not b:
        cache[(a, b)] = []

    elif not a:
        cache[(a, b)] = [b]

    elif not b:
        cache[(a, b)] = [a]

    else:
        result = [a[0]+s for s in interleavings_memoized(a[1:], b)]
        result += [b[0]+s for s in interleavings_memoized(a, b[1:])]
        cache[(a, b)] = result

    return cache[(a, b)]


print(interleavings_memoized(A, B))
