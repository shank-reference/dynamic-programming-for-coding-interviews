class Node:
    def __init__(self, value):
        self.val = value
        self.left = None
        self.right = None

def in_order(node):
    if node.val is None:
        return
    if node.left is not None:
        in_order(node.left)
    print(node.val)
    if node.right is not None:
        in_order(node.right)

