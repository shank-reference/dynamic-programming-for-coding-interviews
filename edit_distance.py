def edit_distance_recursive(s1: str, s2: str) -> int:
    if not s1 and not s2:
        return 0
    if not s1 and s2:
        return len(s2)
    if s1 and not s2:
        return len(s1)

    if s1[0] == s2[0]:
        return edit_distance_recursive(s1[1:], s2[1:])

    d = edit_distance_recursive(s1[1:], s2)     # delete first character of s1
    r = edit_distance_recursive(s1[1:], s2[1:]) # replace first character of s1 with first character of s2
    i = edit_distance_recursive(s1, s2[1:])     # insert first character of s2 in from of s1

    return 1 + min(d, r, i)

S1 = "SATURDAY"
S2 = "SUNDAY"
print(edit_distance_recursive(S1, S2))


cache = {}
def edit_distance_memoized(s1: str, s2: str) -> int:
    if (s1, s2) in cache:
        return cache[(s1, s2)]

    if not s1 and not s2:
        return 0
    if not s1 and s2:
        return len(s2)
    if s1 and not s2:
        return len(s1)


    if s1[0] == s2[0]:
        cache[(s1, s2)] = edit_distance_memoized(s1[1:], s2[1:])
        return cache[(s1, s2)]

    d = edit_distance_memoized(s1[1:], s2)     # delete first character of s1
    r = edit_distance_memoized(s1[1:], s2[1:]) # replace first character of s1 with first character of s2
    i = edit_distance_memoized(s1, s2[1:])     # insert first character of s2 in from of s1

    cache[(s1, s2)] = 1 + min(d, r, i)
    return cache[(s1, s2)]

print(edit_distance_memoized(S1, S2))


def edit_distance_dp(s1: str, s2: str) -> int:
    m = len(s1)
    n = len(s2)
    ed = [[0 for _ in range(n+1)] for _ in range(m+1)]
    for i in range(m+1):
        for j in range(n+1):
            if i == 0:
                ed[0][j] = j

            elif j == 0:
                ed[i][0] = i

            # if the last characters are the same, ignore the last character
            # and recur for the remaining
            elif s1[i-1] == s2[j-1]:
                ed[i][j] = ed[i-1][j-1]

            # if last character are different, consider all possibilities
            # and find the minimum
            else:
                ed[i][j] = 1 + min(
                    ed[i][j-1],     # insert
                    ed[i-1][j],     # remove
                    ed[i-1][j-1]    # replace
                )
    return ed[m][n]

print(edit_distance_dp(S1, S2))
