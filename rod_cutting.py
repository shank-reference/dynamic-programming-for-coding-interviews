from typing import List
from math import inf


def rod_cutting_recursive(l: int, prices: List[int]) -> int:
    if l == 0:
        return 0
    else:
        maxcs = [
            prices[i] + rod_cutting_recursive(l - i, prices)
            for i in range(1, len(prices) + 1) if i <= l
        ]
        return max(maxcs)


R = 4
prices = [None, 1, 5, 6, 9, 10, 17, 17, 20]

print(rod_cutting_recursive(R, prices))


def rod_cutting_dp(l, prices):
    p = len(prices)

    mv = [-inf for _ in range(l + 1)]
    mv[0] = 0

    for i in range(1, l + 1):
        mc = -inf
        for j in range(1, p):
            if j <= i:
                mc = max(mc, prices[j] + mv[i - j])
        mv[i] = mc

    return mv[l]


print(rod_cutting_dp(R, prices))